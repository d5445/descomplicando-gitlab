variable "aws_region" {
  type        = string
  description = "AWS region"
  default     = "us-east-1"
}

variable "aws_keypair_name" {
  type        = string
  description = "Keypair name"
  default     = "gitlab-runner"
}

variable "aws_instance_type" {
  type        = string
  description = "AWS instance type"
  default     = "t2.micro"
}

variable "aws_instance_ami" {
  type        = string
  description = "Instance image/AMI"
  default     = "ami-04505e74c0741db8d"
}

variable "aws_tags" {
  type        = string
  description = "Tags"
  default     = "Descomplicando o GitLab"
}

variable "aws_instance_count" {
  type        = number
  description = "Instance count"
  default     = 1
}