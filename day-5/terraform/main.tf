terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

resource "aws_instance" "my_instance" {
  ami           = var.aws_instance_ami
  instance_type = var.aws_instance_type
  key_name      = var.aws_keypair_name
  count         = var.aws_instance_count

  tags = {
    Name = var.aws_tags
  }
}