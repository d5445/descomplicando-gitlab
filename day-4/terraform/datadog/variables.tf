variable "DD_API_KEY" {
  description = "Datadog API Key"
  type        = string
}

variable "DD_APP_KEY" {
  description = "Datadog APP Key"
  type        = string
}